import { requireAuth } from 'src/router/auth';

const routes = [
  {
    path: '/',
    redirect: '/login', 
  },
  {
    path: '/login',
    component: () => import('layouts/LayoutRegPrijava.vue'),
    children: [
      { path: '', component: () => import('pages/Prijava_m.vue') },
      { path: 'Registracija_m', component: () => import('pages/Registracija_m.vue') },
    ],
  },
  { 
    path: '/trener',
    component: () => import('layouts/MainLayout.vue'),
    beforeEnter: requireAuth,
    children: [
      { path: '', component: () => import('pages/IndexPage.vue') },
      { path: 'PocetnaTrener', component: () => import('pages/PocetnaTrener.vue') },
    ], 
  }, 
  
  {
    path: '/:catchAll(.*)*',
    component: () => import('pages/ErrorNotFound.vue'),
  }
] 

export default routes;